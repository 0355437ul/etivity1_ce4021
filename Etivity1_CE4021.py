# My Problem to solve is to find the dates within a given list of tuples each containing a date and the number of login on that day
# where the application being logged into had an above average number of logins for the provided period.
from datetime import datetime
from dateutil.parser import parse

def getDatesOfAboveAverage(listOfLogins):
    if(isinstance(listOfLogins, (list)) != True):
        print("An error has occured. The input provided was not a list.")
        return
    
    #find the length of the list 
    count = len(listOfLogins)
    if(count == 0):
        print('An Error Has occured. An Empty list has been provided.')
        return

    # set the sum to 0
    sum = 0
    firstDate = listOfLogins[0][0]
    lastDate = firstDate
    
    #loop through the list to find the sum of the number of logins
    #verify if the first elemnt of each tuple is a date and the second is an number
    for tup in listOfLogins:
        try :
            parse(tup[0])
        except ValueError :
                print("The date "+ str(tup[0]) +" is invalid.")
                return
        if(isinstance(tup[1], (int, float)) != True):
            print("The value "+ str(tup[1]) +" is invalid.")
            return
        sum = sum + tup[1]
        if tup[0] < firstDate:
            firstDate = tup[0]
        if tup[0] > lastDate:
            lastDate = tup[0]


    average = sum / count 

    #print the intial message
    print( "For this period of " + str(count) + " days from "+ str(firstDate) + " to " + str(lastDate) +" the average number of logins was " + str(average) + ".")
    print("The days which had an above average number of logins were:")
    # loop through the list to identify the affected dates
    for tup in listOfLogins:
        if tup[1] > average:
            print(str(tup[0]) + ' - ' + str(tup[1]) +' logins' )

    return        



#Hugh Investigate, will my method take a list of lists or tuples?

listOfLogins = [("01/07/2018", 28), ("02/07/2018", 24), ("03/07/2018", 25),("04/07/2018", 26)]

getDatesOfAboveAverage(listOfLogins)